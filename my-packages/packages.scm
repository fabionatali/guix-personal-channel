(define-module (my-packages packages)
  #:use-module (gnu packages autotools)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix licenses)
  #:use-module (guix packages))

(define-public hello-world
  (let ((commit "fb2d77a97020205b8f9b235b03acbf18cbaaa6a7")
        (upstream-version "1.2")
        (guix-version "1"))
    (package
      (name "hello-world")
      (version (git-version upstream-version guix-version commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/fabionatali/hello-world/")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0fngfc7rpm7kl0j04mkn0yl5518xlc1kmyw2pgvy5ai3sbp5249r"))))
      (build-system gnu-build-system)
      (native-inputs
       `(("autoconf" ,autoconf)
         ("automake" ,automake)))
      (home-page "https://gitlab.com/fabionatali/hello-world/")
      (synopsis "Yet another hello-world example app")
      (description "A simple hello-world app to demonstrate how to packaging software under Guix.")
      (license gpl3+))))
